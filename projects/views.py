from django.shortcuts import get_object_or_404, redirect, render
from .models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm
from django.db.models import Q
import json
from datetime import datetime

def project_timeline(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    tasks = Task.objects.filter(project=project).order_by('start_date')

    # Prepare data for the Gantt-like chart
    timeline_data = []
    for task in tasks:
        timeline_data.append({
            'label': task.name,
            'start': task.start_date.isoformat(),
            'end': task.due_date.isoformat(),
        })

    return render(request, 'projects/project_timeline.html', {
        'project': project,
        'timeline_data': json.dumps(timeline_data),
    })

# Create your views here.

@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    return render(request, "projects/list.html", {"projects": projects})


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    return render(request, "projects/show.html", {"project": project})


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            # project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm(initial={"owner": request.user})
    return render(request, "projects/create.html", {"form": form})


def list_projects(request):
    search_query = request.GET.get('search', '')
    projects = Project.objects.all()

    if search_query:
        projects = projects.filter(
            Q(name__icontains=search_query) |
            Q(description__icontains=search_query) |
            Q(owner__username__icontains=search_query)  # Assuming you have an owner field
        )

    context = {
        "projects": projects,
        "search_query": search_query
    }
    return render(request, "projects/list.html", context)
