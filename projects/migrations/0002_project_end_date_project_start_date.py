# Generated by Django 5.0.6 on 2024-06-26 21:26

import datetime
import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="end_date",
            field=models.DateField(
                default=datetime.datetime(
                    2024,
                    7,
                    26,
                    21,
                    26,
                    23,
                    968915,
                    tzinfo=datetime.timezone.utc,
                )
            ),
        ),
        migrations.AddField(
            model_name="project",
            name="start_date",
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
