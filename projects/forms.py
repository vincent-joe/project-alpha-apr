from django import forms
from django.contrib.auth.models import User
from .models import Project


class ProjectForm(forms.ModelForm):
    owner = forms.ModelChoiceField(queryset=User.objects.all())

    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
