from django.urls import path
from projects.views import create_project, list_projects, project_timeline, show_project

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path('timeline/<int:project_id>/', project_timeline, name='project_timeline'),
]
