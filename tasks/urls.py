from django.urls import path
from tasks.views import add_task_notes, create_task, show_my_tasks

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/notes/", add_task_notes, name="add_task_notes"),
]
