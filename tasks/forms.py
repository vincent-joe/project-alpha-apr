from django import forms
from .models import Task


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed"]


class TaskNotesForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ["notes"]
        widgets = {
            "notes": forms.Textarea(attrs={"rows": 4}),
        }
