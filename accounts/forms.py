from django import forms
from django.contrib.auth.models import User


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


class SignupForm(forms.ModelForm):
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )

    class Meta:
        model = User
        fields = ["username", "password"]
        widgets = {"password": forms.PasswordInput}
        help_texts = {"username": None}
